python3 -m venv env

env\Scripts\activate

pip3 install flask

mkdir app

cd .\app\

touch __init__.py
[
    from flask import Flask
    app = Flask(__name__)
    from app import routes
]

