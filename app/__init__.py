from flask import Flask
from app.config import CONF
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
 
app = Flask(__name__)
app.config.from_object(CONF)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
 
from app import routes